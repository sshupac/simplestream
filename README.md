# Sam Shupac - Simplestream Code Challenge

## Installation

Once pulled this repo into local development environment run the following commands to install this app.

1. `composer install`
    
    Installs the app and its dependancies; just Laravel.

2. `php artisan key:generate`
    
    Sets `APP_KEY` inside `.env` file and creates `.env` file if one doesn't exist.

3. Create a database in local MySQL and add connection credentials to `.env` file.

4. `php artisan migrate`

    Setup database structure as required for app to function and store data.

5. `php artisan db:seed`

    Fill the database with dummy data so that API can be tested.


## API Endpoints

### [GET] /api/channels
Lists all channels.

Example Request:
```
/api/channels
```

Example response:

```
{
    "total": 2,
    "channels": [
        {
            "id": "08f34d2a-be15-4f18-8ff2-e553e8c9ad47",
            "name": "Satterfield-Gerhold",
            "icon": "/tmp/be18f045c26e2084508e8f5854e5753f.jpg",
            "created_at": "2020-10-01T23:23:17.000000Z",
            "updated_at": "2020-10-01T23:23:17.000000Z"
        },
        {
            "id": "15bd4148-c640-43bd-a184-43f0616e34bb",
            "name": "Herzog, Streich and Sipes",
            "icon": "/tmp/26940ccc691360f0c61406ad01f80806.jpg",
            "created_at": "2020-10-01T23:23:39.000000Z",
            "updated_at": "2020-10-01T23:23:39.000000Z"
        }
    ]
}
```

### [GET] /api/channels/{channel-uuid}/{date}/timezone/{timezone}
List the upcoming programmes on the given channel on the given date by the given timezone.

Payload:

| Parameter       | Required  | Info                                                                                                |
| ----------------|-----------|-----------------------------------------------------------------------------------------------------|
| `channel-uuid`  | Yes       | Unique identifier for channel                                                                       |
| `date`          | Yes       | Air date of retrieved programmes. Format: `yyyy-mm-dd`                                              |
| `timezone`      | Yes       | Timezone adjustment for programme `start_at` and `end_at` timestamps. Number between `-12` and `12` |

Example Request:
```
/api/channels/210115d1-566b-4731-879b-a1a1ac101b9f/2020-10-02/timezone/2
```

Example Response:
```
{
    "timezone": {
        "timezone": "+02:00",
        "region": "Europe/Paris"
    },
    "channel": {
        "id": "210115d1-566b-4731-879b-a1a1ac101b9f",
        "name": "Hodkiewicz, Swift and Hyatt",
        "icon": "/tmp/bb0635e86a31c81d7cd6f5acc987f9a4.jpg",
        "created_at": "2020-10-01T21:47:45.000000Z",
        "updated_at": "2020-10-01T21:47:45.000000Z"
    },
    "upcoming_programmes": [
        {
            "id": "1707767e-ac7c-46b3-b7aa-bd40cecfb432",
            "channel_id": "210115d1-566b-4731-879b-a1a1ac101b9f",
            "name": "Persevering foreground functionalities",
            "description": "Iure facilis doloremque facere esse voluptatem. Ut iste voluptatem aspernatur est. Est non velit animi numquam est natus.",
            "thumbnail": "/tmp/85d4ba5dc8a919a9e54393cc3c780d0c.jpg",
            "start_at": "2020-10-02T02:22:04.000000Z",
            "duration": 5400,
            "created_at": "2020-10-01T21:47:48.000000Z",
            "updated_at": "2020-10-01T21:47:48.000000Z",
            "end_at": "2020-10-02T03:52:04.000000Z"
        },
        {
            "id": "fb7c9e61-b416-45ee-a441-f7fb4ba7a4ce",
            "channel_id": "210115d1-566b-4731-879b-a1a1ac101b9f",
            "name": "Centralized fault-tolerant processimprovement",
            "description": "Ut voluptas quasi ex nihil natus velit culpa. Aut et illum voluptate corporis numquam laborum eligendi. Qui nemo id quo et. Sit molestiae in modi perferendis.",
            "thumbnail": "/tmp/e41f9a77bb9f3b2c16ee7303336c89e7.jpg",
            "start_at": "2020-10-02T02:33:59.000000Z",
            "duration": 1800,
            "created_at": "2020-10-01T23:11:43.000000Z",
            "updated_at": "2020-10-01T23:11:43.000000Z",
            "end_at": "2020-10-02T03:03:59.000000Z"
        },
        {
            "id": "a0ba443b-e499-42ef-bde8-4179e3a7cf6a",
            "channel_id": "210115d1-566b-4731-879b-a1a1ac101b9f",
            "name": "Networked bottom-line pricingstructure",
            "description": "Excepturi eos sed est omnis. Soluta et quisquam enim nesciunt atque. Facilis similique repellat quia aut eveniet.",
            "thumbnail": "/tmp/b8f77d9b6c079bbd23f26bbad16e3656.jpg",
            "start_at": "2020-10-02T02:39:21.000000Z",
            "duration": 2700,
            "created_at": "2020-10-01T21:47:48.000000Z",
            "updated_at": "2020-10-01T21:47:48.000000Z",
            "end_at": "2020-10-02T03:24:21.000000Z"
        }
    ]
}
```

### [GET] /api/channels/{channel-uuid}/programmes/{programme-uuid}
Get a specific programme on a given channel.

Payload:

| Parameter         | Required  | Info                            |
| ------------------|-----------|---------------------------------|
| `channel-uuid`    | Yes       | Unique identifier for channel   |
| `programme-uuid`  | Yes       | Unique identifier for programme |

Example Request:
```
/api/channels/210115d1-566b-4731-879b-a1a1ac101b9f/programmes/1707767e-ac7c-46b3-b7aa-bd40cecfb432
```

Example Response:
```
{
    "total": 1,
    "programme": {
        "id": "1707767e-ac7c-46b3-b7aa-bd40cecfb432",
        "channel_id": "210115d1-566b-4731-879b-a1a1ac101b9f",
        "name": "Persevering foreground functionalities",
        "description": "Iure facilis doloremque facere esse voluptatem. Ut iste voluptatem aspernatur est. Est non velit animi numquam est natus.",
        "thumbnail": "/tmp/85d4ba5dc8a919a9e54393cc3c780d0c.jpg",
        "start_at": "2020-10-02T00:22:04.000000Z",
        "duration": 5400,
        "created_at": "2020-10-01T21:47:48.000000Z",
        "updated_at": "2020-10-01T21:47:48.000000Z",
        "end_at": "2020-10-02T01:52:04.000000Z",
        "channel": {
            "id": "210115d1-566b-4731-879b-a1a1ac101b9f",
            "name": "Hodkiewicz, Swift and Hyatt",
            "icon": "/tmp/bb0635e86a31c81d7cd6f5acc987f9a4.jpg",
            "created_at": "2020-10-01T21:47:45.000000Z",
            "updated_at": "2020-10-01T21:47:45.000000Z"
        }
    }
}
```

### Extra files

* simplestream_epg.mwb
  
  MySQL Workbench EER diagram file

* simplestream_epg.pdf
  
  PDF version of EER diagram file

* simplestream_epg.postman_collection.json

  JSON config file of API endpoints for Postman app.