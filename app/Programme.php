<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Programme extends Model
{
    use \App\Traits\UsesUuidTrait;
    
    protected $dates = ['start_at'];

    protected $casts = [
        'duration' => 'integer'
    ];

    protected $appends = ['end_at'];

    // attributes

    public function getEndAtAttribute()
    {
        return $this->start_at->addSeconds($this->duration);
    }

    // relations

    public function channel()
    {
        return $this->belongsTo(\App\Channel::class, 'channel_id', 'id');
    }
}
