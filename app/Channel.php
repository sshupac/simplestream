<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Channel extends Model
{
    use \App\Traits\UsesUuidTrait;

    // relations

    public function programmes()
    {
        return $this->hasMany(\App\Programme::class, 'channel_id', 'id');
    }
}
