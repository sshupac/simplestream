<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Programme;

class ProgrammeController extends Controller
{
    public function show($channel_uuid, $programme_uuid)
    {
        $programme = Programme::where('id', $programme_uuid)->where('channel_id', $channel_uuid)->with('channel')->firstOrFail();

        return response()->json([
            "total" => 1,
            "programme" => $programme
        ]);
    }
}
