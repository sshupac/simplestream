<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Channel;

class EPGController extends Controller
{
    public function getChannelTimetableForTimezone($channel_uuid, $date, $timezone)
    {
        if ($timezone > 12 || $timezone < -12) {
            return response()->json(["error" => "Timezone parameter invalid, must be between -12 and 12."], 422);
        }

        $channel = Channel::where('id', $channel_uuid)->firstOrFail();

        $timezone_carbon = new \Carbon\CarbonTimeZone($timezone);

        $upcoming_programmes = $channel->programmes()->orderBy('start_at')->whereDate('start_at', '=', \Carbon\Carbon::parse($date))->get()->each(function ($programme) use ($timezone_carbon) {
            $programme->start_at = $programme->start_at->timezone($timezone_carbon);
            return $programme;
        });

        return response()->json([
            "timezone" => [
                "timezone" => $timezone_carbon->getName(),
                "region" => $timezone_carbon->toRegionTimeZone()->getName()
            ],
            "channel" => $channel,
            "upcoming_programmes" => $upcoming_programmes
        ]);
    }
}
