<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Programme;
use Faker\Generator as Faker;

$factory->define(Programme::class, function (Faker $faker) {
    return [
        'channel_id' => \App\Channel::inRandomOrder()->first()->id,
        'name' => $faker->catchPhrase(),
        'description' => $faker->paragraph(),
        'thumbnail' => $faker->image(),
        'start_at' => $faker->dateTimeBetween('now', '+ 10 hours'),
        'duration' => (collect([30, 45, 60, 90, 120])->random() * 60)
    ];
});
