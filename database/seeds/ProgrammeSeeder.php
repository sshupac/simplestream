<?php

use Illuminate\Database\Seeder;

class ProgrammeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // create 15 programmes and for each one associate it to a random channel
        factory(\App\Programme::class, 15)->create()->each(function ($programme) {
            $programme->channel()->associate(\App\Channel::inRandomOrder()->first());
        });
    }
}
