<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/channels', ['uses' => 'ChannelController@index']);

Route::get('/channels/{channel_uuid}/programmes/{programme_uuid}', ['uses' => 'ProgrammeController@show']);

Route::get('/channels/{channel_uuid}/{date}/timezone/{timezone}', ['uses' => 'EPGController@getChannelTimetableForTimezone']);
