<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ProgrammesTest extends TestCase
{
    use DatabaseTransactions, DatabaseMigrations;

    /**
     * get a programme by uuid for a given channel by uuid
     *
     * @return void
     */
    public function test_get_specific_programme()
    {
        // arrange
        $channel = factory(\App\Channel::class)->create();
        $programme = factory(\App\Programme::class)->create();
        $programme->end_at = $programme->start_at->addSeconds($programme->duration);
        $programme->channel = $channel->toArray();

        // act
        $response = $this->get("/api/channels/{$channel->id}/programmes/{$programme->id}");

        //assert
        $response->assertStatus(200)
            ->assertExactJson([
                "total" => 1,
                "programme" => $programme->toArray()
            ]);
    }
}
