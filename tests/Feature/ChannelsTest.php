<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ChannelsTest extends TestCase
{
    use DatabaseTransactions, DatabaseMigrations;

    /**
     * get a list of all channels
     *
     * @return void
     */
    public function test_get_list_of_channels()
    {
        // arrange
        $channels = factory(\App\Channel::class, 2)->create();

        // act
        $response = $this->get('/api/channels');

        // assert
        $response->assertStatus(200)
            ->assertExactJson([
                "total" => 2,
                "channels" => $channels->toArray()
            ]);
    }
}
