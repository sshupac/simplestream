<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class EPGTest extends TestCase
{
    use DatabaseTransactions, DatabaseMigrations;

    /**
     * get programme timetable for a channel by uuid on a given date for a given timezone
     *
     * @return void
     */
    public function test_get_timetable_for_channel_date_timezone()
    {
        // arrange
        $date = now()->format('Y-m-d');
        $timezone = rand(-5, 5);

        $channel = factory(\App\Channel::class)->create();
        $programmes = factory(\App\Programme::class, 15)->create()->each(function ($programme) use ($channel) {
            $programme->channel()->associate($channel);
            $programme->makeHidden('channel');
        });

        $returned_programmes = $programmes->filter(function ($prog) use ($date, $timezone) {
            if ($prog->start_at->is($date)) {
                $prog->start_at = $prog->start_at->addHours($timezone);
                return $prog;
            }
        });

        $timezone_carbon = new \Carbon\CarbonTimeZone($timezone);

        // act
        $response = $this->get("/api/channels/{$channel->id}/$date/timezone/$timezone");

        // assert
        $response->assertStatus(200)
            ->assertJsonFragment([
                "timezone" => [
                    "timezone" => $timezone_carbon->getName(),
                    "region" => $timezone_carbon->toRegionTimeZone()->getName()
                ]
            ])
            ->assertJsonFragment([
                "channel" => $channel->toArray()
            ])
            ->assertJsonFragment([
                "upcoming_programmes" => $returned_programmes->flatten()->toArray()
            ]);
    }
}
